import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from trananh.items import TrananhItem


class TrananhSpider(Spider):
    name = "trananh"
    start_urls = [
            "http://www.trananh.vn",
        ]
    allowed_domains = ["www.trananh.vn", "trananh.vn"]
    list_temp_url = ["http://www.trananh.vn"]


    # def start_requests(self):
    #     yield scrapy.Request('http://www.vienthonga.vn', callback=self.parse_link)


    def parse(self, response):

        # Get all link
        # print(len(response.selector.css("a::attr('href')").extract()))
        for link in response.selector.css("a::attr('href')").extract():
            link = response.urljoin(link)
            if ( link[0:21] == 'http://www.trananh.vn' or link[0:17] == 'http://trananh.vn') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 

                

                
        # print self.list_temp_url




    def parse_link(self, response):
        # Get all link in website
        for link in response.selector.css('a::attr("href")').extract():
            link = response.urljoin(link)
            if link[0:24] == 'http://www.vienthonga.vn' and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_link)
        print len(self.list_temp_url)


    def parse_detail(self, response):

        for link in response.selector.css("a::attr('href')").extract():
            link = response.urljoin(link)
            if ( link[0:21] == 'http://www.trananh.vn' or link[0:17] == 'http://trananh.vn') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 

        # print response.selector.css('.detail-main-image img::attr("src")').extract()
        
        item = PicoItem()
        item['name'] = response.selector.css('h1 div#ctl00_ContentPlaceHolder1_ltltitleproduct::text').extract()
                
        if item['name'] != []:
            item['shop'] = 'trananh'
            item['price'] = response.selector.css('.sidebar-box-content.sidebar-padding-box.product-single-info .price::text').extract()
            item['link'] = response.url
            item['image'] = response.selector.css('#product-slider img::attr("src")').extract()
            yield item 


        # l = ItemLoader(FptshopItem(), response)
        # l.add_css('name', '.detail-name::text')
        # l.add_css('main_image', '.detail-main-image img::attr("src")')
        # # l.add_css('other_images', '')
        # l.add_css('description', '.detail-main-specification')

        # yield l.load_item()
