import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from fptshop.items import FptshopItem


class FptSpider(Spider):
    name = "FptShop"
    start_urls = [
            "http://fptshop.com.vn",
            # "http://fptshop.com.vn/dien-thoai",
            # "http://fptshop.com.vn",
        ]
    allowed_domains = ["fptshop.com.vn"]
    list_temp_url = ["http://fptshop.com.vn"]


    # def start_requests(self):
    #     yield scrapy.Request('http://fptshop.com.vn/may-tinh-bang', callback=self.parse_detail)

    def parse(self, response):

        # Get all link
        # print(len(response.selector.css("a::attr('href')").extract()))
        for link in response.selector.css("a::attr('href')").extract():
            if link[0:21] == 'http://fptshop.com.vn' and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail)
        # print self.list_temp_url


    def parse_detail(self, response):

        for link in response.selector.css("a::attr('href')").extract():
            if link[0:21] == 'http://fptshop.com.vn' and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail)

        # print response.selector.css('.detail-main-image img::attr("src")').extract()

        item = FptshopItem()
        item['name'] = response.selector.css('.detail-name::text').extract()
        
        if item['name'] != []:
            item['shop'] = 'fptshop'
            item['image'] = response.selector.css('.detail-main-image img::attr("src")').extract()
            item['price'] = response.selector.css('.detail-current-price').extract()
            item['link'] = response.url
            yield item


        # l = ItemLoader(FptshopItem(), response)
        # l.add_css('name', '.detail-name::text')
        # l.add_css('main_image', '.detail-main-image img::attr("src")')
        # # l.add_css('other_images', '')
        # l.add_css('description', '.detail-main-specification')

        # yield l.load_item()
