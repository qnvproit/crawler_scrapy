import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from pico.items import PicoItem, LinkItem, ProductionItem


class AdayroiSpider(Spider):
    name = "adayroi"
    start_urls = [
            "http://politics.people.com.cn/n/2015/1024/c1001-27734815.html",
        ]
    allowed_domains = ["http://politics.people.com.cn", "politics.people.com.cn"]
    list_temp_url = ["https://www.adayroi.com"]


    # def start_requests(self):
    #     yield scrapy.Request('https://img.adayroi.com/resize/70_70/100/2015/6/9/91145_lenovos41059434419_1.jpg', callback=self.parse_test)


    def parse(self, response):

        if not hasattr(response, 'selector'):
            return

        print str(response.selector.css("#p_content").extract()).decode('unicode-escape')

        # # Get all link
        # for link in response.selector.css("a::attr('href')").extract():
        #     link = response.urljoin(link).split('#')[0]
        #     if ( link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'http://adayroi.com') and self.list_temp_url.count(link) == 0:
        #         self.list_temp_url.append(link)
        #         yield scrapy.Request(link, self.parse_detail) 




    # def parse_link(self, response):

    #     if not hasattr(response, 'selector'):
    #         return

    #     # Get all link in website
    #     for link in response.selector.css('a::attr("href")').extract():
    #         link = response.urljoin(link).split('#')[0]

    #         if self.list_temp_url.count(link) == 0:
    #             item = LinkItem()
    #             item['link'] = link
    #             yield item

    #             self.list_temp_url.append(link)

    #             if link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'https://adayroi.com':
    #                 yield scrapy.Request(link, self.parse_link) 



    def parse_detail(self, response):
        # IMPORTANT !!
        # Must check ( response == null ? ) before loop through link
        # Ex. link is url of a image --> response == null --> Not exists 'selector' attribute --> loop after it will be broken because of exception --> missing valid links what after this link
        if not hasattr(response, 'selector'):
            return

        for link in response.selector.css("a::attr('href')").extract():
            link = response.urljoin(link).split('#')[0]
            if ( link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'http://adayroi.com') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 

        item = ProductionItem()
        item['name'] = response.selector.css('.item-info-block h1.item-title::text').extract()
                
        if item['name'] != []:
            item['shop'] = 'adayroi'
            item['price'] = response.selector.css('#item_prices div.item-price::text').extract()
            item['link'] = response.url
            item['image'] = response.selector.css('.stage img::attr("src")').extract()
            yield item 

