import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from pico.items import ProductionItem, LinkItem

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class PicoSpider(Spider):
    name = "sishust"
    start_urls = [
            "http://sis.hust.edu.vn/",
        ]
    allowed_domains = ["http://sis.hust.edu.vn/", "http://sis.hust.edu.vn/ModuleGradeBook/StudentCourseMarks.aspx"]
    # list_temp_url = ["https://hoanghamobile.com"]


    # def start_requests(self):
    #     yield scrapy.Request('http://www.pico.vn', callback=self.parse_link)

    def __init__(self):
        self.driver = webdriver.Firefox()

    def parse(self, response):

        if not hasattr(response, 'selector'):
            return

        self.driver.get(response.url)



        ##############
        # username = self.driver.find_element_by_css_selector('#ctrl_pageLogin_login')
        # username.send_keys('qnvproit@gmail.com')

        # password = self.driver.find_element_by_css_selector('#ctrl_pageLogin_password')
        # password.send_keys('2521993dhhhtb')
        # password.send_keys(Keys.ENTER)
        # # btn = self.driver.find_element_by_css_selector('.primary')
        # # btn.click()

        # done_name = self.driver.find_element_by_css_selector('.accountUsername')
        # print done_name.text
        ##############



        username = self.driver.find_element_by_css_selector('#cLogIn1_tb_cLogIn_User_I')
        username.send_keys('20112025')

        password = self.driver.find_element_by_css_selector('#cLogIn1_tb_cLogIn_Pass_I')
        password.send_keys('')
        password.send_keys(Keys.ENTER)

        # done_name = self.driver.find_element_by_css_selector('.status_text strong')
        # print done_name.text

        # self.driver.Navigation.to("http://sis.hust.edu.vn/ModuleGradeBook/StudentCourseMarks.aspx")

        self.driver.get('http://sis.hust.edu.vn/ModuleGradeBook/StudentCourseMarks.aspx')

        done_name = self.driver.find_element_by_css_selector('#mainTextHeader')
        print done_name.text

        # yield scrapy.Request('http://sis.hust.edu.vn/ModuleGradeBook/StudentCourseMarks.aspx', callback=self.parse_point)






        # Get all link
        # print(len(response.selector.css("a::attr('href')").extract()))
        # for link in response.selector.css("a::attr('href')").extract():
            
        #     link = response.urljoin(link).split('#')[0]

        #     if ( link[0:25] == 'https://hoanghamobile.com' or link[0:29] == 'https://www.hoanghamobile.com') and self.list_temp_url.count(link) == 0:
        #         self.list_temp_url.append(link)
        #         yield scrapy.Request(link, self.parse_detail) 
      

    def parse_point(self, response):
        my_class = self.driver.find_element_by_css_selector('.dxgvTitlePanel_SisTheme')
        print my_class.text



    def parse_detail(self, response):

        if not hasattr(response, 'selector'):
            return

        for link in response.selector.css("a::attr('href')").extract():
            link = response.urljoin(link).split('#')[0]
            if ( link[0:25] == 'https://hoanghamobile.com' or link[0:29] == 'https://www.hoanghamobile.com') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail)  

        item = ProductionItem()
        item['name'] = response.selector.css('.left h1 strong::text').extract()
                
        if item['name'] != []:
            item['shop'] = 'hoanghamobile'
            item['price'] = response.selector.css('.product-price p span::text').extract()
            item['link'] = response.url
            item['image'] = response.selector.css('#slider1_container div img::attr("src")').extract()
            yield item
