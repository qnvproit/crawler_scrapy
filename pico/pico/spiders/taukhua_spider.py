import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from pico.items import NomItem

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class TauKhuaSpider(Spider):
    name = 'taukhua'
    allowed_domains = ["http://www.nomfoundation.org"]

    start_urls = ["http://www.nomfoundation.org/vn/du-an-nom/Kho-chu-Han-Nom-ma-hoa/Bang-doi-chieu-Han-Nom-Quoc-ngu"]

    def __init__(self):
        self.driver = webdriver.Firefox()

    def parse(self, response):
        if not hasattr(response, 'selector'):
            return

        self.driver.get(response.url)

        tables = self.driver.find_elements_by_css_selector('form table table table')

        # crawl current page
        for table in tables:
            trs = table.find_elements_by_css_selector('tr')
            for index, tr in enumerate(trs):
                if index != 0:
                    tds = tr.find_elements_by_css_selector('td')

                    item = NomItem()

                    temp = tds[0].text.split('U+')

                    # print str(len(tds[0].text.split('U+'))) + "--" + str(len(tds[0].text.split('V+')))
                    if len(temp) == 1:
                        item['code'] = ("V+" + tds[0].text.split('V+')[1]).strip()
                    else:
                        item['code'] = ("U+" + temp[1]).strip()


                    item['text'] = tds[1].text.strip()

                    yield item


        # go to next page
        # for i in range(337):
        #     self.driver.execute_script("GotoPage(" + str(i) + ");");
        #     tables = self.driver.find_elements_by_css_selector('form table table table')
        #     for table in tables:
        #         trs = table.find_elements_by_css_selector('tr')
        #         for index, tr in enumerate(trs):
        #             if index != 0:
        #                 tds = tr.find_elements_by_css_selector('td')

        #                 item = NomItem()

        #                 temp = tds[0].text.split('U+')

        #                 # print str(len(tds[0].text.split('U+'))) + "--" + str(len(tds[0].text.split('V+')))
        #                 if len(temp) == 1:
        #                     item['code'] = ("V+" + tds[0].text.split('V+')[1]).strip()
        #                 else:
        #                     item['code'] = ("U+" + temp[1]).strip()


        #                 item['text'] = tds[1].text.strip()

        #                 yield item
        
        


