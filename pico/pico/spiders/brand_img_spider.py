import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from pico.items import PicoItem, LinkItem


class BrandSpider(Spider):
    name = "brand"
    start_urls = [
            "http://websosanh.vn/samsung/cat-88.htm",
        ]
    allowed_domains = ["http://websosanh.vn", "websosanh.vn"]
    list_temp_url = ["http://www.websosanh.vn"]


    # def start_requests(self):
    #     yield scrapy.Request('http://www.pico.vn', callback=self.parse_link)


    def parse(self, response):

        if not hasattr(response, 'selector'):
            return


        print "HELLO"
        # Get all link
        print(len(response.selector.css(".partner-item img").extract()))
        for img in response.selector.css(".partner-item img::attr('src')").extract():
            
            print img
            


    # def parse_link(self, response):
    #     if not hasattr(response, 'selector'):
    #         return

    #     # Get all link in website
    #     for link in response.selector.css('a::attr("href")').extract():
    #         link = response.urljoin(link).split('#')[0]

    #         if self.list_temp_url.count(link) == 0:
    #             item = LinkItem()
    #             item['link'] = link
    #             yield item

    #             self.list_temp_url.append(link)

    #             if link[0:18] == 'http://www.pico.vn' or link[0:14] == 'http://pico.vn':
    #                 yield scrapy.Request(link, self.parse_link) 
                    

    def parse_detail(self, response):

        if not hasattr(response, 'selector'):
            return

        for link in response.selector.css("a::attr('href')").extract():
            link = response.urljoin(link).split('#')[0]
            if ( link[0:18] == 'http://www.pico.vn' or link[0:14] == 'http://pico.vn') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 

        item = PicoItem()
        item['name'] = response.selector.css('.page-noneback h1#Home_ContentPlaceHolder_Product_Control_head_Title::text').extract()
                
        if item['name'] != []:
            item['shop'] = 'pico'
            item['price'] = response.selector.css('.sidebar-box-content.sidebar-padding-box.product-single-info .price::text').extract()
            item['link'] = response.url
            item['image'] = response.selector.css('#product-slider img::attr("src")').extract()
            yield item
