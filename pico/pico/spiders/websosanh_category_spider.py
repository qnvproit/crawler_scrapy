import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from pico.items import CategoryItem, TrainingItem


class WebsosanhSpider(Spider):
    name = "cate_websosanh"
    start_urls = [
        "http://websosanh.vn/"
    ]
    allowed_domains = ["www.websosanh.vn", "websosanh.vn"]
    list_temp_url = ["http://www.websosanh.vn"]

    list_url_category = []

    temp_parent = ""

    def start_requests(self):
        yield scrapy.Request('http://websosanh.vn/', callback=self.parse_category_level_1)

    def parse_category_level_1(self, response):
        if not hasattr(response, 'selector'):
            return

        for li in response.css('.container .box-cate h3'):
            # Get link
            link = li.css('a::attr("href")').extract()[0]
            link = response.urljoin(link)

            # Get name 
            # item = CategoryItem()
            # item['name'] = li.css('a::text').extract()[0]
            # item['level'] = 1
            # item['parent'] = None
            # item['link'] = link

            
            yield scrapy.Request(link, self.parse_sub_category)
            # yield item


    def parse_category_sub_level(self, response):
        if not hasattr(response, 'selector'):
            return


        for li in response.css('.middle-top .breadCrumb li.current'):
            
            # Get parent
            parent = li.css('a:nth-child(1)::text').extract()[0].strip()

            temp = len(li.css('ul li'))
            if temp == 0:
                print "--------------------------------------------"

                link = response.url

                yield scrapy.Request(link, self.parse_data)

                # Get number result
                number = int(response.css('.middle-top .total-result::text').extract()[0].replace('.', ''))

                max_page = min(50, number/30) - 2

                for i in range(max_page):
                    temp_link = link.replace('.htm', '') + "/trang-" + str(i + 2) + ".htm"
                    yield scrapy.Request(temp_link, self.parse_data)

                continue

            for subli in li.css('ul li'):

                # Get link 
                link = subli.css('a::attr("href")').extract()[0]
                link = response.urljoin(link)

                # Get name
                # item = CategoryItem()
                # item['name'] = subli.css('a::text').extract()[0]
                # item['parent'] = parent
                # item['link'] = link
                # item['level'] = 2

                yield scrapy.Request(link, self.parse_category_sub_level)

                # yield item
        # link = response.url
        # if isLeaf:
        #     yield scrapy.Request(link, self.parse_list_page)


    def parse_sub_category(self, response):
        if not hasattr(response, 'selector'):
            return

        is_leaf = 1

        # Get current category
        current_cate = response.css('.middle-top .breadCrumb li.current>a::text').extract()[0].strip()

        # Get parent category
        path_cates = response.css('.middle-top>.breadCrumb>ul>li')
        if len(path_cates) > 2:
            parent_item = path_cates[len(path_cates) - 2]
            parent_cate = parent_item.css('a::text').extract()[0].strip()
            current_cate = parent_cate + "___" + current_cate

        # Get link 
        link = response.css('.middle-top .breadCrumb li.current>a::attr("href")').extract()[0].strip()
        link = response.urljoin(link)

        


        for link in response.css('.middle-top .breadCrumb li.current ul li a::attr("href")').extract():
            is_leaf = 0
            yield scrapy.Request(link, self.parse_sub_category)

        # Get name
        item = CategoryItem()
        item['name'] = current_cate
        item['link'] = link
        item['is_leaf'] = is_leaf

        yield item



    def parse_category_level_2(self, response):
        if not hasattr(response, 'selector'):
            return

        isLeaf = True


        for li in response.css('.middle-top .breadCrumb li.current'):
            
            # Get parent
            parent = li.css('a:nth-child(1)::text').extract()[0].strip()
            temp = len(li.css('ul li'))
            if temp == 0:
                # is Leaf
                link = response.url

                yield scrapy.Request(link, self.parse_data)

                # Get number result
                number = int(response.css('.middle-top .total-result::text').extract()[0].replace('.', ''))

                print number

                max_page = min(50, number/30) - 2

                for i in range(max_page):
                    temp_link = url.replace('.htm', '') + "/trang-" + str(i + 2) + ".htm"
                    yield scrapy.Request(temp_link, self.parse_data)

                continue

            for subli in li.css('ul li'):
                isLeaf = False

                # Get link 
                link = subli.css('a::attr("href")').extract()[0]
                link = response.urljoin(link)

                # Get name
                # item = CategoryItem()
                # item['name'] = subli.css('a::text').extract()[0]
                # item['parent'] = parent
                # item['link'] = link
                # item['level'] = 2

                yield scrapy.Request(link, self.parse_category_level_3)

                # yield item
        # link = response.url
        # if isLeaf:
        #     yield scrapy.Request(link, self.parse_list_page)


    def parse_category_level_3(self, response):
        if not hasattr(response, 'selector'):
            return

        isLeaf = True


        for li in response.css('.middle-top .breadCrumb li.current'):
            

            # Get parent
            parent = li.css('a:nth-child(1)::text').extract()[0].strip()
            temp = len(li.css('ul li'))
            if temp == 0:
                link = response.url
                yield scrapy.Request(link, self.parse_list_page)
                continue

            for subli in li.css('ul li'):
                isLeaf = False
                
                # Get link 
                link = subli.css('a::attr("href")').extract()[0]
                link = response.urljoin(link)

                # Get name
                # item = CategoryItem()
                # item['name'] = subli.css('a::text').extract()[0]
                # item['parent'] = parent
                # item['link'] = link
                # item['level'] = 3

                yield scrapy.Request(link, self.parse_category_level_4)

                # yield item
        # link = response.url
        # if isLeaf:
        #     yield scrapy.Request(link, self.parse_list_page)


    def parse_category_level_4(self, response):
        if not hasattr(response, 'selector'):
            return

        isLeaf = True


        for li in response.css('.middle-top .breadCrumb li.current'):
            

            # Get parent
            parent = li.css('a:nth-child(1)::text').extract()[0].strip()

            temp = len(li.css('ul li'))
            if temp == 0:
                link = response.url
                yield scrapy.Request(link, self.parse_list_page)
                continue

            for subli in li.css('ul li'):
                isLeaf = False
                
                # Get link 
                link = subli.css('a::attr("href")').extract()[0]
                link = response.urljoin(link)

                # Get name
                # item = CategoryItem()
                # item['name'] = subli.css('a::text').extract()[0]
                # item['parent'] = parent
                # item['link'] = link
                # item['level'] = 4

                yield scrapy.Request(link, self.parse_list_page)

                # yield item
        # link = response.url
        # if isLeaf:
        #     yield scrapy.Request(link, self.parse_list_page)



    def parse_list_page(self, response):
        if not hasattr(response, 'selector'):
            return

        url = response.url

        yield scrapy.Request(url, self.parse_data)

        # Get number result
        number = int(response.css('.middle-top .total-result::text').extract()[0].replace('.', ''))

        max_page = min(50, number/30) - 2

        for i in range(max_page):
            link = url.replace('.htm', '') + "/trang-" + str(i + 2) + ".htm"
            yield scrapy.Request(link, self.parse_data)




    def parse_data(self, response):
        if not hasattr(response, 'selector'):
            return

        # Get category
        temp = response.css('.middle-top h1.search-key::text').extract()[0].strip()

        # Get parent category
        path_cates = response.css('.middle-top>.breadCrumb>ul>li')
        parent_item = path_cates[len(path_cates) - 2]
        parent_cate = parent_item.css('a::text').extract()[0].strip()



        for li in response.css('.list-product table tbody tr'):
            # Get name
            item = TrainingItem()
            item['category'] = parent_cate + "___" + temp
            item['name'] = li.css('h3 a::text').extract()[0].strip()

            yield item



            # Next page
