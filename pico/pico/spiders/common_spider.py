import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from pico.items import PicoItem, LinkItem, ProductionItem


class CommonSpider(Spider):
    name = "common"
    start_urls = [
            "https://www.adayroi.com",
        ]
    allowed_domains = ["www.adayroi.com", "adayroi.com"]
    list_temp_url = ["https://www.adayroi.com/dien-thoai-pho-thong-c327?p=1&s=10&brand=5404"]

    tag_item = ".body-list-item div"
    tag_title = "h4.post-title a::text"
    tag_page = "div.pagination a.item::attr('href')"

    f = open("data2.txt", "w")

    def start_requests(self):
        yield scrapy.Request('https://www.adayroi.com/dien-thoai-pho-thong-c327?p=1&s=10&brand=5404', callback=self.parse)


    def parse(self, response):

        if not hasattr(response, 'selector'):
            return

        # Mark done
        self.list_temp_url.append(response.url)

        # Get name of Production
        for name in response.selector.css(self.tag_title).extract():
            self.f.write((name + ", ").encode('utf8'))

        # Get all link page
        for link in response.selector.css(self.tag_page).extract():
            link = response.urljoin(link).split('#')[0]
            if self.list_temp_url.count(link) == 0:
                yield scrapy.Request(link, self.parse)

        print (self.list_temp_url)




    # def parse_link(self, response):

    #     if not hasattr(response, 'selector'):
    #         return

    #     # Get all link in website
    #     for link in response.selector.css('a::attr("href")').extract():
    #         link = response.urljoin(link).split('#')[0]

    #         if self.list_temp_url.count(link) == 0:
    #             item = LinkItem()
    #             item['link'] = link
    #             yield item

    #             self.list_temp_url.append(link)

    #             if link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'https://adayroi.com':
    #                 yield scrapy.Request(link, self.parse_link) 



    def parse_detail(self, response):
        # IMPORTANT !!
        # Must check ( response == null ? ) before loop through link
        # Ex. link is url of a image --> response == null --> Not exists 'selector' attribute --> loop after it will be broken because of exception --> missing valid links what after this link
        if not hasattr(response, 'selector'):
            return

        for link in response.selector.css("a::attr('href')").extract():
            link = response.urljoin(link).split('#')[0]
            if ( link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'http://adayroi.com') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 

        item = ProductionItem()
        item['name'] = response.selector.css('.item-info-block h1.item-title::text').extract()
                
        if item['name'] != []:
            item['shop'] = 'adayroi'
            item['price'] = response.selector.css('#item_prices div.item-price::text').extract()
            item['link'] = response.url
            item['image'] = response.selector.css('.stage img::attr("src")').extract()
            yield item 

