import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from pico.items import CategoryItem


class CategorySpider(Spider):
    name = "category"
    start_urls = [
        "https://www.adayroi.com/"
    ]
    allowed_domains = ["www.adayroi.com", "adayroi.com"]
    list_temp_url = ["https://www.adayroi.com"]

    list_url_category = []

    temp_parent = ""

    str_response = ""


    def start_requests(self):
        yield scrapy.Request('https://www.adayroi.com/', callback=self.parse_category_level_1)


    def parse_category_level_1(self, response):
        if not hasattr(response, 'selector'):
            return

        order = 0;
        # Get all link
        for li in response.selector.css("ul#mega_menu_0_list li"):
            order += 1
            if order == 1 or order == 12:
                continue

            link = li.css('a::attr("href")').extract()[0]
            link = response.urljoin(link)
            if self.list_url_category.count(link) == 0:
                item = CategoryItem()
                item['name'] = li.css('.title::text').extract()[0].lower()
                item['level'] = 1
                item['parent'] = None
                item['link'] = link
                


                self.list_url_category.append(link);

                temp_parent = item['name']
                yield scrapy.Request(link, self.parse_category_level_2)

                yield item


            # print li.css('.title::text').extract()


            # link = response.urljoin(link).split('#')[0]
            # if ( link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'http://adayroi.com') and self.list_temp_url.count(link) == 0:
            #     self.list_temp_url.append(link)
            #     yield scrapy.Request(link, self.parse_detail) 


    def parse_category_level_2(self, response):
        # if not hasattr(response, 'selector'):
        #     return

        for li in response.selector.css(".left-sidebar #dvProductFilterBox div"):
            print li
            # link = li.css('a::attr("href")').extract()[0]
            # link = response.urljoin(link)
            # if self.list_url_category.count(link) == 0:
            #     item = CategoryItem()
            #     item['name'] = li.css('a::text').extract()[0].lower()
            #     item['level'] = 2
            #     item['parent'] = self.temp_parent
            #     item['link'] = link

            #     self.list_url_category.append(link);

            #     temp_parent = item['name']
            #     # yield scrapy.Request(link, self.parse_category_level_3)

            #     yield item


    def parse_category_level_3(self, response):
        if not hasattr(response, 'selector'):
            return


    def parse_category_level_4(self, response):
        if not hasattr(response, 'selector'):
            return

