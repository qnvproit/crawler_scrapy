# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class ProductionItem(scrapy.Item):
    name = scrapy.Field()

    price = scrapy.Field()

    image = scrapy.Field()

    link = scrapy.Field()

    shop = scrapy.Field()


class PicoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = scrapy.Field()

    price = scrapy.Field()

    image = scrapy.Field()

    link = scrapy.Field()

    shop = scrapy.Field()


class HoangHaItem(scrapy.Item):
    name = scrapy.Field()

    price = scrapy.Field()

    image = scrapy.Field()

    link = scrapy.Field()

    shop = scrapy.Field()    

    
class CategoryItem(scrapy.Item):
    name = scrapy.Field()

    level = scrapy.Field()

    parent = scrapy.Field()

    link = scrapy.Field()

    is_leaf = scrapy.Field()   
 

class TrainingItem(scrapy.Item):
    name = scrapy.Field()
    category = scrapy.Field()


class LinkItem(scrapy.Item):
    link = scrapy.Field()


class NomItem(scrapy.Item):
    code = scrapy.Field()
    text = scrapy.Field()
