import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from mapping.items import MappingItem


class MappingSpider(Spider):
    name = "adayroi"
    start_urls = [
            "https://www.adayroi.com/dien-thoai-di-dong-m323?brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=2&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=3&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=4&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=5&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=6&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=7&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=8&brand=1130",
            "https://www.adayroi.com/dien-thoai-di-dong-m323?p=9&brand=1130",
        ]
    allowed_domains = ["https://www.adayroi.com", "adayroi.vn", "https://adayroi.com"]
    list_temp_url = ["https://www.adayroi.com"]


    # def start_requests(self):
    #     yield scrapy.Request('https://img.adayroi.com/resize/70_70/100/2015/6/9/91145_lenovos41059434419_1.jpg', callback=self.parse_test)


    def parse(self, response):

        if not hasattr(response, 'selector'):
            return

        for prod in response.selector.css(".body-list-item div"):
            item = MappingItem()
            item['name'] = prod.css('.post-title a::text').extract()
                    
            if item['name'] != []:
                item['shop'] = 'adayroi'
                item['price'] = prod.css('.item-price span::text').extract()
                # item['link'] = prod.url
                yield item



    # def parse_link(self, response):

    #     if not hasattr(response, 'selector'):
    #         return

    #     # Get all link in website
    #     for link in response.selector.css('a::attr("href")').extract():
    #         link = response.urljoin(link).split('#')[0]

    #         if self.list_temp_url.count(link) == 0:
    #             item = LinkItem()
    #             item['link'] = link
    #             yield item

    #             self.list_temp_url.append(link)

    #             if link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'https://adayroi.com':
    #                 yield scrapy.Request(link, self.parse_link) 



    # def parse_detail(self, response):
    #     # IMPORTANT !!
    #     # Must check ( response == null ? ) before loop through link
    #     # Ex. link is url of a image --> response == null --> Not exists 'selector' attribute --> loop after it will be broken because of exception --> missing valid links what after this link
    #     if not hasattr(response, 'selector'):
    #         return

    #     list1 = response.selector.css(".category-child a::attr('href')").extract()
    #     list2 = response.selector.css(".pagination a::attr('href')").extract()


    #     list_links = list1 + list2

    #     # # Get all link
    #     for link in list_links:
    #         link = response.urljoin(link).split('#')[0]
    #         if ( link[0:18] == 'http://www.pico.vn' or link[0:14] == 'http://pico.vn') and self.list_temp_url.count(link) == 0:
    #             self.list_temp_url.append(link)
    #             yield scrapy.Request(link, self.parse_detail) 



    #     item = MappingItem()
    #     item['name'] = response.selector.css('.page-noneback h1#Home_ContentPlaceHolder_Product_Control_head_Title::text').extract()
                
    #     if item['name'] != []:
    #         item['shop'] = 'pico'
    #         item['price'] = response.selector.css('.sidebar-box-content.sidebar-padding-box.product-single-info .price::text').extract()
    #         item['link'] = response.url
    #         yield item

