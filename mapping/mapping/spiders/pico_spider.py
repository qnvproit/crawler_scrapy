import scrapy
from scrapy.spiders import Spider
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor

from mapping.items import MappingItem


class MappingSpider(Spider):
    name = "pico"
    start_urls = [
            "http://pico.vn/dien-thoai-di-dong-nhom-75.html?&Filter=:63:",
        ]
    allowed_domains = ["http://pico.vn", "pico.vn", "http://www.pico.vn"]
    list_temp_url = ["https://www.pico.com"]


    # def start_requests(self):
    #     yield scrapy.Request('https://img.adayroi.com/resize/70_70/100/2015/6/9/91145_lenovos41059434419_1.jpg', callback=self.parse_test)


    def parse(self, response):

        if not hasattr(response, 'selector'):
            return


        list1 = response.selector.css(".category-child a::attr('href')").extract()
        list2 = response.selector.css(".pagination a::attr('href')").extract()


        list_links = list1 + list2

        # # Get all link
        for link in list_links:
            link = response.urljoin(link).split('#')[0]
            if ( link[0:18] == 'http://www.pico.vn' or link[0:14] == 'http://pico.vn') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 




    # def parse_link(self, response):

    #     if not hasattr(response, 'selector'):
    #         return

    #     # Get all link in website
    #     for link in response.selector.css('a::attr("href")').extract():
    #         link = response.urljoin(link).split('#')[0]

    #         if self.list_temp_url.count(link) == 0:
    #             item = LinkItem()
    #             item['link'] = link
    #             yield item

    #             self.list_temp_url.append(link)

    #             if link[0:23] == 'https://www.adayroi.com' or link[0:19] == 'https://adayroi.com':
    #                 yield scrapy.Request(link, self.parse_link) 



    def parse_detail(self, response):
        # IMPORTANT !!
        # Must check ( response == null ? ) before loop through link
        # Ex. link is url of a image --> response == null --> Not exists 'selector' attribute --> loop after it will be broken because of exception --> missing valid links what after this link
        if not hasattr(response, 'selector'):
            return

        list1 = response.selector.css(".category-child a::attr('href')").extract()
        list2 = response.selector.css(".pagination a::attr('href')").extract()


        list_links = list1 + list2

        # # Get all link
        for link in list_links:
            link = response.urljoin(link).split('#')[0]
            if ( link[0:18] == 'http://www.pico.vn' or link[0:14] == 'http://pico.vn') and self.list_temp_url.count(link) == 0:
                self.list_temp_url.append(link)
                yield scrapy.Request(link, self.parse_detail) 



        item = MappingItem()
        item['name'] = response.selector.css('.page-noneback h1#Home_ContentPlaceHolder_Product_Control_head_Title::text').extract()
                
        if item['name'] != []:
            item['shop'] = 'pico'
            item['price'] = response.selector.css('.sidebar-box-content.sidebar-padding-box.product-single-info .price::text').extract()
            item['link'] = response.url
            yield item

