import scrapy
from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.loader import ItemLoader

from tinhte.items import TinhteItem


class TinhteSpider(Spider):
    name = 'tinhtespider'
    start_urls = ['http://www.tinhte.vn']
    allowed_domains = ['tinhte.vn']
    list_news = []

    def start_requests(self):
        yield scrapy.Request('https://www.tinhte.vn'. self.parse_first_news)
        yield scrapy.Request('https://www.tinhte.vn/?wpage=2', self.parse_first_news)
        yield scrapy.Request('https://www.tinhte.vn/?wpage=3', self.parse_first_news)

    def parse(self, response):
        # item = response.selector.xpath("//div[@class='newsBlocks']/div/div[@class='section sectionMain recentNews'][2]/div[@class='primaryContent leftDate']//a[@class='newsTitle']/text()")
        # # item = response.selector.css(".recentNews:nth-of-type(1) .primaryContent")
        # print item.extract()
        # for item in response.selector.xpath("//div[@class='primaryContent leftDate']"):
        #     news = TinhteItem()
        #     news['title'] = item.xpath(".//a[@class='newsTitle']/text()").extract()
        #     news['author'] = item.xpath(".//a[@class='username']/text()").extract()
        #     news['created_time'] = item.xpath(".//a[2]/text()").extract()
        #     print news
        #     break

        l = ItemLoader(item=TinhteItem(), response=response)
        l.add_xpath('title', "//div[@class='newsBlocks']/div/div[@class='section sectionMain recentNews']/div[@class='primaryContent leftDate']//a[@class='newsTitle']/text()")
        # l.add_xpath('title', "//div[@class='newsBlocks']/div/div[@class='section sectionMain recentNews'][2]/div[@class='primaryContent leftDate']//a[@class='newsTitle']/text()")
        l.add_xpath('author', '//div[@class="primaryContent leftDate"]//a[@class="username"]/text()')
        l.add_xpath('created_time', '//div[@class="primaryContent leftDate"]//a[2]/text()')
        print l.load_item()

    def parse_first_news(self, response):
        print response.selector.xpath("//div[@class='newsBlocks']/div/div[@class='section sectionMain recentNews'][1]/div[@class='primaryContent leftDate']//a[@class='newsTitle']/text()").extract()

