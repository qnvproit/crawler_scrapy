# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item
from scrapy.loader.processors import Join, MapCompose, TakeFirst


def my_fun(input):
    return input


class TinhteItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field(
        input_processor = MapCompose(my_fun),
        # output_processor = TakeFirst(),
    )
    author = scrapy.Field(
        input_processor = MapCompose(my_fun),
        # output_processor = TakeFirst()
    )
    created_time = scrapy.Field(
        input_processor = MapCompose(my_fun),
        # output_processor = TakeFirst()
    )
