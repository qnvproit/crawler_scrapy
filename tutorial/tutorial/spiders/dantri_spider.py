import scrapy


class DantriSpider(scrapy.Spider):

    """ Scrapy will use Spider to scrape information from some domains
    """
    name = 'dantri'  # Name of this spider
    allowed_domains = ['www.dantri.com.vn']  # Allowed domains
    start_urls = [
        'http://dantri.com.vn/su-kien/thu-tuong-tra-loi-chat-van-ve-phong-trao-xay-quang-truong-hoanh-trang-20150805093314564.htm', 
    ]   # list url to crawling

    def parse(self, response):
        self.logger.info('Ha Noi %s', response.url)
